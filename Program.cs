﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba_3
{
    class Program
    {
        static void Main(string[] args)
        {
            object[,] Users = new object[,] { { 33580156631, 189956, 11, 00, "Shreck Tercero", new DateTime(2020, 7, 1, 18, 53, 0) },
                { 49085300227, 178442, 22, 01, "Jhon Whick", new DateTime(2020, 7, 1, 10, 30, 0) }, 
                { 95841267746, 963852, 33, 01, "Tim Pool", new DateTime(2020, 7, 2, 13, 21, 0) }};
            int largo = (Users.GetLength(0));
            // 11=supervisor; 22=administrador; 33=vendedor
            // 00=inactivo; 01=activo

            bool UserExiste = false;
            bool PwExiste = false;
            bool Admicion = false;
            int PosicionUsuario = 0;

            while (Admicion == false)
            {

                Console.WriteLine("Usuario:");
                long Usuario = Convert.ToInt64(Console.ReadLine());
                Console.WriteLine("Contraseña:");
                long Password = Convert.ToInt32(Console.ReadLine());

                for (int i = 1; i <= largo; i++)
                {
                    if (Usuario == ((long)Users[i-1, 0]))
                    {
                        UserExiste = true;

                        if (Password== (int)Users[i-1,1])
                        {
                            PwExiste = true;
                            PosicionUsuario = i-1;
                        }
                    }
                }

                if (UserExiste == false || PwExiste == false)
                {
                    Console.WriteLine();
                    Console.WriteLine("Usuario o contraseña incorectas");
                    Console.WriteLine("Intente nuevamente");
                    Console.WriteLine();
                }
                else if ((int)Users[PosicionUsuario, 3] == 00)
                {
                    Console.WriteLine();
                    Console.WriteLine("Usuario inactivo, intente entrar con otro");
                    Console.WriteLine();
                }
                else
                {
                    Admicion = true;
                }
            }
            string Rol = "";
            switch (Users[PosicionUsuario, 2])
            {
                case (11):
                    Rol = "Supervisor";
                    break;
                case (22):
                    Rol = "Administrador";
                    break;
                case (33):
                    Rol = "Vendedor";
                    break;
                default:
                    Rol = "Ninguno";
                    break;
            }

            Console.WriteLine();
            Console.WriteLine($"Acabas de ingresar con el usuario {Users[PosicionUsuario, 0]} y su rol es {Rol}");
            Console.WriteLine($"Bienvenido {Users[PosicionUsuario,4]}");

            Console.ReadKey();
        }
    }
}
